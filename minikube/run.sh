minikube start --memory 8192 --cpus 4 --mount --mount-string="/spark_storage:/spark_storage"
minikube addons enable metrics-server
