echo "Starting Minikube..."
./run.sh

#echo "Building Containers in minikube env (quietly)..."
#./build_worker.sh > /dev/null

echo "Starting Master pod..."
./master_start.sh

echo "Starting Worker pods and Ingress..."
kubectl create -f ./spark-worker-deployment.yaml

#minikube addons enable ingress
#kubectl apply -f ./minikube-ingress.yaml
