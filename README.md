### Tutos
## AWS et python
 
 - API Python pour AWS : https://aws.amazon.com/sdk-for-python/ <-- "boto3"
 - Tuto "elastic kubernetes service" : https://docs.aws.amazon.com/code-samples/latest/catalog/code-catalog-python-example_code-eks.html

## Kubernetes & co
 - Docker --> Kubernetes : https://kubernetes.io/blog/2019/07/23/get-started-with-kubernetes-using-python/
 - https://gruntwork.io/guides/kubernetes/how-to-deploy-production-grade-kubernetes-cluster-aws/#interacting-with-kubernetes
 - https://images.contentstack.io/v3/assets/blt300387d93dabf50e/bltec54ae56e6302d11/5a21ccde473ff3867b91653f/wc-kube-aws-flat.png

### Rapport

Sections : automatisation, déploiement (de hadoop), déploiement (d'outils de monitoring)
