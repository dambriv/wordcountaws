#!/bin/bash
#unset var set by kubernetes
unset SPARK_MASTER_PORT

#expects master ip in /etc/hosts as "spark-master"
if ! getent hosts spark-master; then
  echo No master, exiting...
  sleep 5
  
  exit 0
fi

$SPARK_HOME/bin/spark-class org.apache.spark.deploy.worker.Worker spark://spark-master:7077 --webui-port 8081