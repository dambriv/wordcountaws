# whatitdo

Worker : /worker

Télécharge hadoop, jdk-8 & spark depuis le site d'hagimont

Extrait tout et met tout ce qu'il faut dans le PATH (comme vu dans l'intro du TP Spark scalability)

# TODO

 - Changer "master" dans core-site.xml (hadoop-2.7.1/etc/hadoop/core-site.xml)
 - Changer "slave1" "slave2" dans slaves (hadoop-2.7.1/etc/hadoop/slaves)

 - Changer "slave1" "slave2" dans slaves (spark-.../conf/slaves)
 - Changer "master" dans spark-env.sh (spark-.../conf/spark-env.sh)

Adapter la conf de spark : https://spark.apache.org/docs/latest/running-on-kubernetes.html
