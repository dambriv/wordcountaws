import boto3
from botocore.exceptions import ClientError

ec2 = boto3.client('ec2')
ec2bis = boto3.resource('ec2')

response = ec2.describe_vpcs()
vpc_id = response.get('Vpcs', [{}])[0].get('VpcId', '')

try:
    response = ec2.delete_security_group(GroupName='Kubernetes')

    response = ec2.create_security_group(GroupName='Kubernetes',Description='Communication entre les membres du cluster',VpcId=vpc_id)
    print(response)
    security_group_id = response['GroupId']

    data = ec2.authorize_security_group_ingress(
        GroupId=security_group_id,
        IpPermissions=[
            {'IpProtocol': '-1',
             'FromPort': -1,
             'ToPort': -1,
             'IpRanges': [{'CidrIp': '0.0.0.0/0'}]}
        ])

    instances = ec2bis.instances.all()
    for instance in instances:
        instance.modify_attribute(Groups=[security_group_id])

except ClientError as e:
    print(e)
