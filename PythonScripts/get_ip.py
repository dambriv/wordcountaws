import boto3



def getIp():
    publicIpList=[]
    privateIpList=[]
    ec2 = boto3.client('ec2')
    running_instances = ec2.describe_instances()
    for reserv in running_instances['Reservations']:
        for inst in reserv['Instances']:
            for inter in inst['NetworkInterfaces']:
                privateIpList.append(inter.get('PrivateIpAddresses')[0].get('PrivateIpAddress'))
                if inter.get('Association')!=None:
                    publicIpList.append(inter.get('Association').get('PublicIp')) 

    return publicIpList,privateIpList

## --------------------------------------------------------------------------------------
## Config sys :

## dans ~/.aws/credentials :
## [default]
## aws_access_key_id = YOUR_ACCESS_KEY
## aws_secret_access_key = YOUR_SECRET_KEY

## dans ~/.aws/config :
## [default]
## region=eu-west-3

## conda install boto3
