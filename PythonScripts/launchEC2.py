import boto3
import sys
from get_ip import getIp
from botocore.exceptions import ClientError
from collections import defaultdict
import fileinput
import time

NUM_VM=3

## Instance d'un client EC2
ec2 = boto3.client('ec2')

def runInstance(FILE_NAME):

    ## Lancement d'une instance avec un script d'init file name
    try:
        response = ec2.run_instances(ImageId='ami-0d3f551818b21ed81',
                                     KeyName='ec2-keypair',
                                     InstanceType='t2.xlarge',
                                     MinCount=1,
                                     MaxCount=1,
                                     SecurityGroups=['Kubernetes','default'],
                                     UserData=open(FILE_NAME).read())
        for instance in response['Instances']:
            print(instance['InstanceId'])
            id = instance['InstanceId']
    
    except ClientError as e:
        print(e)




#Run Master
runInstance("../BashScripts/012-master.sh")
time.sleep(15)
MASTER_IP = getIp()[0][0]
print(MASTER_IP)


time.sleep(90)

def write_cfg(slaveNum,ipMaster):
    fin = open("../BashScripts/013-slave.sh", "rt")
    fout = open("../BashScripts/"+str(slaveNum)+"013-slave.sh", "wt")
    for line in fin:
        fout.write(line.replace('nfs-slave','!!!').replace('slave','slave'+str(slaveNum)).replace('ipMaster',ipMaster).replace('!!!','nfs-slave'))
    fin.close()
    fout.close()


#Run Slaves
for i in range(1,NUM_VM):
    print('RUN SLAVE : ',str(i))
    write_cfg(i,MASTER_IP)
    runInstance("../BashScripts/"+str(i)+"013-slave.sh")


## Monitor
#ec2.monitor_instances(InstanceIds=[id])
time.sleep(15)
print(getIp()[0])



## --------------------------------------------------------------------------------------
## Config sys :

## dans ~/.aws/credentials :
## [default]
## aws_access_key_id = YOUR_ACCESS_KEY
## aws_secret_access_key = YOUR_SECRET_KEY

## dans ~/.aws/config :
## [default]
## region=eu-west-3

## conda install boto3

 
