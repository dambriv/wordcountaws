import boto3
import sys
from botocore.exceptions import ClientError


instance_id = sys.argv[1]


## Instance ec2
ec2 = boto3.client('ec2')

try:
    response = ec2.terminate_instances(InstanceIds=[instance_id], DryRun=False)
    print(response)
except ClientError as e:
    print(e)