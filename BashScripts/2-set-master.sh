#!/bin/bash
#run as root


if [ "$EUID" -ne 0 ]
	then echo "Please run as root"
	exit
fi

#Managing NFS
apt install nfs-kernel-server -y
mkdir /IOshared
echo "/IOshared *(rw,no_root_squash)" >> /etc/exports
/etc/init.d/nfs-kernel-server reload
/etc/init.d/nfs-kernel-server start




#parsing of the kubeadm join command"
kubeadm init --ignore-preflight-errors=NumCPU,Mem,SystemVerification --pod-network-cidr=10.0.0.0/16 > /IOshared/kubectl.res 2>&1


mkdir -p /root/.kube
cp -i /etc/kubernetes/admin.conf /root/.kube/config
chown $(id -u):$(id -g) /root/.kube/config
export HOME=/root
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

tail -n 2 /IOshared/kubectl.res > /IOshared/kubectl.sh
chmod +x /IOshared/kubectl.sh
