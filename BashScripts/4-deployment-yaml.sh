#!/bin/bash
#Usage : 4-deployment-yaml.sh app | e.g. [ ./4-deployment-yaml.sh XXXXX ] deploys files named XXXXX-deployment.yaml and XXXXX-service.yaml
# These files must be stored in a directory named XXXXX.

work_dir=/home/ubuntu/wordcountaws

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

kubectl create -f $work_dir/$1/$1-deployment.yaml
kubectl create -f $work_dir/$1/$1-service.yaml
