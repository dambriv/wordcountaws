#!/bin/bash
#Usage : 0-init.sh hostname ipMaster ipSlave1 ipSlave2 .... etc.| hostname=master or slave1 or slave2

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


hostname=$1
ipmaster=$2
hostnamectl set-hostname $hostname
echo "$ipmaster master" >> /etc/hosts


for i in $(seq 3 $#); do
  echo "${!i} slave$((i-2))" >> /etc/hosts
done

apt update -y
swapoff -a
