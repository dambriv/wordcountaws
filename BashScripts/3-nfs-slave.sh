#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

apt install nfs-common -y

export HOME=/root
mkdir /IOshared
mount -o rw -t nfs master:/IOshared /IOshared
/IOshared/kubectl.sh > /IOshared/err.txt 2>&1
sleep 60
/IOshared/kubectl.sh > /IOshared/err2.txt 2>&1
