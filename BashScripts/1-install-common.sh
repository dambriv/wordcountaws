#!/bin/bash
#run as root
work_dir=/home/ubuntu/wordcountaws/BashScripts
#Depends on 
#	$work_dir/k8sconf/k8s.conf
#   $work_dir/k8sconf/kubernetes.list



if [ "$EUID" -ne 0 ]
	then echo "Lance en root"
	exit
fi


#Docker DL
wget -qO- https://get.docker.com/ | sh

#Kubernetes
modprobe br_netfilter
cp $work_dir/k8sconf/k8s.conf /etc/sysctl.d/k8s.conf
sysctl --system
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
cp $work_dir/k8sconf/kubernetes.list /etc/apt/sources.list.d/kubernetes.list
apt update -y 
apt install kubelet kubeadm kubectl -y



